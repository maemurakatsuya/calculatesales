package jp.alhinc.maemura.katsuya.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.FilenameFilter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CalculateSales {

	public static void main(String[] args) {

		try {
			Map<String, String> branchMap = new HashMap<String, String>();
			Map<String, Long> salesMap = new HashMap<String, Long>();
			BufferedReader br = null;

			try {
				File file = new File(args[0], "branch.lst");
				if (!file.exists()) {
					System.out.println("支店定義ファイルが存在しません");
					return;
				}
				br = new BufferedReader(new FileReader(file));

				String line;
				while ((line = br.readLine()) != null) {

					String[] branchInfo = line.split(",");
					String code = branchInfo[0];
					String name = branchInfo[1];

					if (!branchInfo[0].matches("[0-9]{3}") || (branchInfo.length != 2)) {
						System.out.println("支店定義ファイルのフォーマットが不正です");
						return;
					}
					branchMap.put(code, name);
					salesMap.put(branchInfo[0], 0L);
				}
			} finally {
				if (br != null) {
					br.close();
				}
			}

			File dir = new File(args[0]);

			//listFilesメソッドを使用して一覧を取得する
			File[] list = dir.listFiles();

			if (list != null) {

				//"rcd"が含まれるフィルタを作成する
				FilenameFilter filter = new FilenameFilter() {
					public boolean accept(File file, String str) {
						// 拡張子を指定する
						if (str.matches("[0-9]{8}.rcd")) {
							return true;
						} else {
							return false;
						}
					}
				};

				try {
					File[] files = new File(args[0]).listFiles(filter);
					//ファイル一覧を昇順に並び替え
					Arrays.sort(files);

					for (int i = 0; i < files.length; i++) {

						Integer salesFiles = Integer.parseInt(files[i].getName().substring(0, 8));
						Integer minFile = Integer.parseInt(files[0].getName().substring(0, 8));

						if (salesFiles != minFile + i ) {
							System.out.println("売上ファイル名が連番になっていません");
							return;
						}

						br = new BufferedReader(new FileReader(files[i]));

						List<String> salesAggregate = new ArrayList<String>();
						String sales;
						while ((sales = br.readLine()) != null) {
							salesAggregate.add(sales);
						}

						if (salesAggregate.size() != 2) {
							System.out.println(files[i].getName() + "のフォーマットが不正です");
							return;
						}

						String storeCode = salesAggregate.get(0);
						Long storeSales = Long.parseLong(salesAggregate.get(1));

						if (!branchMap.containsKey(storeCode)) {
							System.out.println(files[i].getName() + "の支店コードが不正です。");
							return;
						}

						long storeTotal = salesMap.get(storeCode) + storeSales;

						if (Long.toString(storeTotal).length() > 10) {
							System.out.println("合計金額が10桁を超えました");
							return;
						}

						salesMap.put(storeCode,storeTotal);

					}
				} finally {
					if (br != null) {
						br.close();
					}
				}

				BufferedWriter bw = null;

				try {
					File outFile = new File(args[0], "branch.out");
					bw = new BufferedWriter(new FileWriter(outFile));

					for (Map.Entry<String, String> branch : branchMap.entrySet()) {
						bw.write(branch.getKey() + "," + branch.getValue() + "," + salesMap.get(branch.getKey()));
						bw.newLine();
					}
				} finally {
					if (bw != null) {
						bw.close();
					}
				}
			}
		} catch (Exception e) {
			System.out.println("予期せぬエラーが発生しました。");
			e.printStackTrace();
			return;
		}
	}
}
